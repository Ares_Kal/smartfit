﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartFit.Models
{
    [Serializable]
    public class Food
    {
        public Food()
        {
            Meals = new ObservableCollection<Meal>();
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }
        public float Calories { get; set; }
        public float Protein { get; set; }
        public float CarboHydrates { get; set; }

        public virtual ObservableCollection<Meal> Meals { get; set; }
    }
}
