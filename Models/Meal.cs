﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartFit.Models
{
    [Serializable]
    public enum MealType
    {
        Breakfast,
        Lunch,
        Snak,
        Dinner
    }
    [Serializable]
    public class Meal
    {

        public Meal()
        {
            Foods = new ObservableCollection<Food>();
            DailyDiets = new ObservableCollection<DailyDiet>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        public MealType? MealType { get; set; }

        public virtual ObservableCollection<Food> Foods { get; set; }
        public virtual ObservableCollection<DailyDiet> DailyDiets { get; set; }
    }
}
