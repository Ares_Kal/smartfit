﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartFit.Models
{
    [Serializable]
    public enum WeightGoal
    {
        Lose,
        Keep,
        Gain
    }
    [Serializable]
    public class Diet
    {
        public Diet()
        {
            DailyDiets = new ObservableCollection<DailyDiet>();
            Start = DateTime.Now;
            Finish = DateTime.Now.AddDays(7);
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? Finish { get; set; }

        public WeightGoal? WeightGoal { get; set; }
        public float Calories { get; set; }
        public float Protein { get; set; }
        public float CarboHydrates { get; set; }

        public virtual ObservableCollection<DailyDiet> DailyDiets { get; set; }
    }
}
