﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartFit.Models
{
    [Serializable]
    public class DailyDiet
    {
        public DailyDiet()
        {
            Meals = new ObservableCollection<Meal>();
            Diets = new ObservableCollection<Diet>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }
        public virtual ObservableCollection<Meal> Meals { get; set; }
        public virtual ObservableCollection<Diet> Diets { get; set; }
    }
}
