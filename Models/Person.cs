﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartFit.Models
{
    [Serializable]
    public class Person
    {
        public Person()
        {
            Diets = new ObservableCollection<Diet>();
            Birthday = DateTime.Now;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime Birthday { get; set; }
        public float Height { get; set; }
        public float Weight { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode {get;set;}

        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        
        public string Notes { get; set; }
       // public virtual ICollection<Food> LEatingHabbits { get; set; }
        //public virtual ICollection<Food> DEatingHabbits { get; set; }

        public virtual ObservableCollection<Diet> Diets { get; set; }
    }
}
