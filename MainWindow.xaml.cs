﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
namespace SmartFit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public bool IsAuth = false;
        public string Username = "";
        public MainWindow()
        {
            InitializeComponent();
            
        }


        private void MetroWindow_ContentRendered(object sender, EventArgs e)
        {
           
        }

        private void LoginUI()
        {
            lblUsername.Visibility = System.Windows.Visibility.Visible;
            btnLogin.Visibility = System.Windows.Visibility.Hidden;
            btnLogin.Width = 0;
            lblUsername.Content = Username;
            lWCmds.Visibility = System.Windows.Visibility.Visible;
            grdApp.Visibility = System.Windows.Visibility.Visible;
            grdApp.IsEnabled = true;
        }

        private void LogoutUI()
        {
            lblUsername.Visibility = System.Windows.Visibility.Hidden;
            btnLogin.Visibility = System.Windows.Visibility.Visible;
            btnLogin.Width = lblUsername.Width;
            lblUsername.Content = "";
            lWCmds.Visibility = System.Windows.Visibility.Hidden;
            grdApp.Visibility = System.Windows.Visibility.Hidden;
            grdApp.IsEnabled = false;
        }
        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            LoginDialogSettings loginsettings = new LoginDialogSettings();
            loginsettings.FirstAuxiliaryButtonText = "Options";
            loginsettings.NegativeButtonText = "Cancel";
            loginsettings.NegativeButtonVisibility = System.Windows.Visibility.Visible;
            do
            {
                LoginDialogData result = await this.ShowLoginAsync("Authentication", "Enter your credentials", loginsettings);
                if (result == null)
                {
                    break;
                }
                else
                {
                    // 
                    using (var db = new Models.DB())
                    {

                        var u = from x in db.Users
                                where x.Password == result.Password && x.Username == result.Username
                                select x;
                        if (u != null && u.Count() == 1)
                        {
                            IsAuth = true;
                            Username = result.Username;
                        }
                        else
                        {
                            MessageDialogResult messageResult = await this.ShowMessageAsync("Authentication Error", "Wrong username or password.\nPlease try again.");

                        }

                    }
                }

                if (IsAuth)
                {
                    LoginUI();
                }
            } while (!IsAuth);
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            flSettings.IsOpen = !flSettings.IsOpen;
        }

        private void flSettings_IsOpenChanged(object sender, EventArgs e)
        {
            if (flSettings.IsOpen)
            {
                var sett = SmartFit.Properties.Settings.Default;
                txtDatabase.Text = sett.Database;
                txtPassword.Text = sett.Password;
                txtServer.Text = sett.Server;
                txtUserId.Text = sett.UserID;
            }
        }

        private async void btnSettings_Save_Click(object sender, RoutedEventArgs e)
        {
            prSettSave.IsActive = true;
            var sett = SmartFit.Properties.Settings.Default;
            sett.Database = txtDatabase.Text;
            sett.Password = txtPassword.Text;
            sett.Server = txtServer.Text;
            sett.UserID = txtUserId.Text;
            sett.ConnStr = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};",
                sett.Server, sett.Database, sett.UserID, sett.Password);
            sett.Save();
            string error = null;
            try
            {
                using (Models.DB db = new Models.DB())
                {
                    if (db.Database.CreateIfNotExists())
                    {
                        db.Database.Initialize(true);
                        db.Users.Add(new Models.User() { Username = "admin", Password = "admin" });
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                error = ex.Message;
          }
            if(error!=null){
             MessageDialogResult messageResult = await this.ShowMessageAsync("Settings Error","Cannot connect to database.\n" + error);
            }
            prSettSave.IsActive = false;
            LogoutUI();
        }

  
    }
}
