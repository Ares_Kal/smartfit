﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartFit.Forms
{
    /// <summary>
    /// Interaction logic for sfListView.xaml
    /// </summary>
    public partial class sfListView : UserControl
    {
        public sfListView()
        {
            InitializeComponent();
        }

        public virtual void Init()
        {

        }

        public virtual void OnAddClicked(object sender, RoutedEventArgs e)
        {

        }

        public virtual void OnEditClicked(object sender, RoutedEventArgs e)
        {

        }

        public virtual void OnRefreshClicked(object sender, RoutedEventArgs e)
        {

        }

        public virtual void OnDeleteClicked(object sender, RoutedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            OnAddClicked(sender, e);
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            OnEditClicked(sender, e);
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            OnDeleteClicked(sender, e);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            OnRefreshClicked(sender, e);
        }


    }
}
