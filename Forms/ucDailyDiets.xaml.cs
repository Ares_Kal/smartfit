﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SmartFit.Models;

namespace SmartFit.Forms
{
    /// <summary>
    /// Interaction logic for ucDailyDiets.xaml
    /// </summary>
    public partial class ucDailyDiets : UserControl
    {
        public ObservableCollection<Meal> Meals { get; set; }
        public ObservableCollection<DailyDiet> DailyDiets { get; set; }

        Models.DB db;
        public ucDailyDiets()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            DailyDiets.Add(new DailyDiet());
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null)
                DailyDiets.Remove(MainDG.SelectedItem as DailyDiet);
        }

        private void btnDAdd_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null && cb1.SelectedIndex >= 0)
            {
                (MainDG.SelectedItem as DailyDiet).Meals.Add(cb1.SelectedItem as Meal);
            }
        }

        private void btnDRemove_Click(object sender, RoutedEventArgs e)
        {
            if (DetailDG.SelectedItem != null)
                (MainDG.SelectedItem as DailyDiet).Meals.Remove(DetailDG.SelectedItem as Meal);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            Meals = new ObservableCollection<Meal>(from x in db.Meals select x);
            DailyDiets = new ObservableCollection<DailyDiet>(from x in db.DailyDiets.Include("Meals")
                                                   select x);
            MainDG.ItemsSource = DailyDiets;

            cb1.ItemsSource = Meals;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            db = new Models.DB();
            RefreshData();
            DailyDiets.CollectionChanged += DailyDiets_CollectionChanged;
        }

        void DailyDiets_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (DailyDiet dd in e.NewItems)
                    db.DailyDiets.Add(dd);

            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (DailyDiet dd in e.OldItems)
                {
                    if ((from x in db.DailyDiets where x.ID == dd.ID select x).Count() > 0)
                        db.DailyDiets.Remove(dd);
                }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            db.Dispose();

        }
    }
}
