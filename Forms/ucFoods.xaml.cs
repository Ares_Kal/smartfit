﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SmartFit.Models;

namespace SmartFit.Forms
{
    /// <summary>
    /// Interaction logic for ucFoods.xaml
    /// </summary>
    public partial class ucFoods : UserControl
    {
        ObservableCollection<Food> Foods = new ObservableCollection<Food>();
        Models.DB db;
        public ucFoods()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Foods.Add(new Food());
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null)
                Foods.Remove(MainDG.SelectedItem as Food);
        }


        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            Foods = new ObservableCollection<Food>(from x in db.Foods
                                                   select x);
            MainDG.ItemsSource = Foods;

        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            db = new Models.DB();
            RefreshData();
            Foods.CollectionChanged += Foods_CollectionChanged;
        }

        void Foods_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (Food food in e.NewItems)
                    db.Foods.Add(food);

            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (Food food in e.OldItems)
                {
                    if ((from x in db.Foods where x.ID == food.ID select x).Count() > 0)
                    db.Foods.Remove(food);
                }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            db.Dispose();
        }
    }
}
