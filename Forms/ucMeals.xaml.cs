﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SmartFit.Models;

namespace SmartFit.Forms
{
    /// <summary>
    /// Interaction logic for ucMeals.xaml
    /// </summary>
    public partial class ucMeals : UserControl
    {
        public ObservableCollection<Meal> Meals;
        public ObservableCollection<Food> FoodsCol { get; set; }
        Models.DB db;
        public ucMeals()
        {
            InitializeComponent();
        }



        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Meals.Add(new Meal());
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null)
                Meals.Remove(MainDG.SelectedItem as Meal);
        }

        private void btnDAdd_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null && cb1.SelectedIndex >= 0)
            {
                (MainDG.SelectedItem as Meal).Foods.Add(cb1.SelectedItem as Food);
            }
        }

        private void btnDRemove_Click(object sender, RoutedEventArgs e)
        {
            if (DetailDG.SelectedItem != null)
                (MainDG.SelectedItem as Meal).Foods.Remove(DetailDG.SelectedItem as Food);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            FoodsCol = new ObservableCollection<Food>(from x in db.Foods select x);
            Meals = new ObservableCollection<Meal>(from x in db.Meals.Include("Foods")
                                                   select x);
            MainDG.ItemsSource = Meals;

            cb1.ItemsSource = FoodsCol;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            db = new Models.DB();
            RefreshData();
            Meals.CollectionChanged += Meals_CollectionChanged;
        }

        void Meals_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (Meal m in e.NewItems)
                    db.Meals.Add(m);

            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (Meal m in e.OldItems)
                {
                    if ((from x in db.Meals where x.ID == m.ID select x).Count() > 0)
                        db.Meals.Remove(m);
                }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            db.Dispose();
            
        }
    }
}
