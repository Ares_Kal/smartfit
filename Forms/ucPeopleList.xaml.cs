﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SmartFit.Models;

namespace SmartFit.Forms
{
    /// <summary>
    /// Interaction logic for ucPeopleList.xaml
    /// </summary>
    public partial class ucPeopleList : UserControl
    {
        ObservableCollection<Person> Persons;
        public ObservableCollection<DailyDiet> DailyDiets { get; set; }
        Models.DB db;
        public ucPeopleList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Persons.Add(new Person());
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedItem != null)
                Persons.Remove(MainDG.SelectedItem as Person);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
                db.SaveChanges();
            //RefreshData();
        }

        void RefreshData()
        {
            Persons = new ObservableCollection<Person>(from x in db.Persons.Include("Diets").Include("Diets.DailyDiets")
                                                       select x);
            DailyDiets = new ObservableCollection<DailyDiet>(from x in db.DailyDiets select x);
            MainDG.ItemsSource = Persons;
            cb1.DataContext = this;
            cb1.ItemsSource = DailyDiets;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            db = new DB();
            RefreshData();
            Persons.CollectionChanged +=Persons_CollectionChanged;
        }
        void Persons_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (Person p in e.NewItems)
                    db.Persons.Add(p);

            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (Person p in e.OldItems)
                {
                    if ((from x in db.Persons where x.ID == p.ID select x).Count() > 0)
                        db.Persons.Remove(p);
                }
        }
        private void btnMDAdd_Click(object sender, RoutedEventArgs e)
        {
            if (MainDG.SelectedIndex >= 0)
            {
                (MainDG.SelectedItem as Person).Diets.Add(new Diet());
            }
        }

        private void btnMDRemove_Click(object sender, RoutedEventArgs e)
        {
            if (MainDGDiet.SelectedIndex >= 0)
            {
                (MainDG.SelectedItem as Person).Diets.Remove((MainDGDiet.SelectedItem as Diet));
            }
        }

        private void btnDRemove_Click(object sender, RoutedEventArgs e)
        {
            if (DetailDGD.SelectedIndex >= 0)
            {
                (MainDGDiet.SelectedItem as Diet).DailyDiets.Remove(DetailDGD.SelectedItem as DailyDiet);
            }
        }

        private void btnDAdd_Click(object sender, RoutedEventArgs e)
        {
            if (MainDGDiet.SelectedIndex >= 0 && cb1.SelectedIndex>=0)
            {
                (MainDGDiet.SelectedItem as Diet).DailyDiets.Add(cb1.SelectedItem as DailyDiet);
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            db.Dispose();
        }

 

        
    }
}
